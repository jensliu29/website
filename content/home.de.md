+++
title = "Home"
url = "home"
+++

<!--Two-Space indentation is necessary here 🤮🤮 -->

<div id="sponsors">
  <h1>Sponsoren</h1>
  <p>Ohne die großzügigen Sponsoren, würde die Entwicklung von Redox OS nicht mehr weitergehen können. Wir sind allen unten aufgelisteten Sponsoren sehr dankbar.</p>
  
  <div class="row">
    <p>Noch niemand hier <a href="/donate">Sei der erste!</a></p>
  </div>
  
  <a href="./donate"><button class="donate">Spenden</button></a>
  
  <h2>Einzelne Sponsoren</h2>
  <p>Zu dieser Liste gehört natürlich auch jeder, der über die Jahre spendiert hat. Wir danken euch herzlich!</p>
</div>

<hr />

<div>
  <h1>Was ist Redox</h1>
  
  <p>Redox ist ein freies, offenes Betriebssystem, in Rust geschrieben.</p>
  <p>Eine Mikrokern Architektur ermöglicht uns auf Sicherheit, und Stabilität zu fokusieren.</p>
  
  <a href="#learn-more"><button class="external">Mehr erfahren</button></a>
  <a href="https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md"><button class="gitlab">Loslegen!</button></a>
  <a href="/donate"><button class="donate">Spenden</button></a>
</div>

<div class="advantage">
  <h1 id="learn-more">Von Rust verteidigt</h1>
  <h2>Eliminiert Bugs</h2>
  <p>Die Komplilierzeit Sicherheitsmechanismen des Rust Compilers und beschränkter Syntax vernichten Bugs.</p>

  <div class="card">
    <h2>Wiederstandsfähiger gegen Speicherkorruption</h2>
    <article>
      <p>Der Rust Compiler macht es fast unmöglich, Speicher zu korruptieren und macht Redox dadurch viel verlässlicher und stabiler</p>
    </article>
  </div>
  
  <h2>Bessere Sicherheit</h2>
  <p>Speichersicherheitsprobleme können die Basis von bis zu 70% aller Sicherheitslücken<a><sup>2</sup></a> sein. Rust verhindert diese, und macht Redox dadurch sicherer.</p>
  
  <div class="card">
    <h2>Verischert mehere Aufgaben</h2>
    <article>
      <p>
        C/C++ kann bei mehreren gleichzeitigen Aufgaben in Schwierigkeiten kommen. 
        Dies kann schwierig-zu-erkennende Fehler oder Fehlerquellen verursachen.
        Mit Rust sind diese Fehler erkannt bevor sie gerfährlich werden können.
      </p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>Eine Mikrokern Architektur</h1>
  <div class="card">
    <h2>Mehr Sicherheit durch beschränkte Rechte</h2>
    <article>
      <p>
        Ein Mikrokern ist nahezu die kleinste Software die alle Mechanismen bereitstellen kann, um ein vollständiges Betriebssystem implementieren zu können, die mit den höchsten Rechten des Prozessors ausgeführt werden.
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>Stabilität durch Isolierung</h2>
    <article>
      <p>
        Die Systemkomponente die in der Benutzerebene laufen, sind vom Kern getrennt. Daher können die meisten Fehler nie das gesammte System beinflussen.
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>Modularisch und Neustart-loses Design</h2>
    <article>
      <p>
        Ein stabiler Mikrokern verändert sich selten, und Neustarten zu müssten ebenso. 
        Die meisten Systemkomponente liegen in der Benutzerebene und können dadurch im Laufen ausgetauscht werden, ohne dass ein Neustart erforderlich ist. 
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>Einfach zum entwickeln und pflegen</h2>
    <article>
      <p>Redox ist pflegeleicht da Systemkomponente getrennt vom Kern sind.</p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>Linux apps on Redox</h1>
  <div class="card">
    <h2>Relibc: Die Linuxbrücke zu Redox OS</h2>
    <article>
      <p>Relibc ist eine besondere Bibliothek die es ermöglicht, Linuxsoftware auf Redox auszuführen. Es bildet eine Brücke, zwischen den Sprachen von Linux und Redox.</p>
    </article>
  </div>
  
  <div class="card">
    <h2>Wachsende Kompatibilität zwischen Redox und Linux</h2>
    <article>
      <p>Aktuell laufen hunderte von Programme und Bibliotheken ohne weitere Aufwand. Über 1 000 Programme und Bibliotheken werden gerade zu Redox geportet.</p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>Open-Source und Frei</h1>
  
  <div class="card">
    <h2>Benutze beliebige Software</h2>
    <article>
      <p>Nutze Redox um beliebige Software auszuführen und benutzen, ohne dich um Lizenzen kümmern zu müssen.</p>
    </article>
  </div>

  <div class="card">
    <h2>Behebe Fehler oder melde sie</h2>
    <article>
      <p>Alle können die Qualität und Sicherheit vom Redox Quelltext durchlesen, verbessern oder Fehler an die Entwickler melden.</p>
    </article>
  </div>
  
  <div class="card">
    <h2>Frei und bleibt immer so</h2>
    <article>
      <p>Keine Firmen besitzen oder kontrollieren Redox. Redox ist eine gemeinnützige Software, sie sich nur mit Spenden am Leben hält.</p>
    </article>
  </div>
</div>

<!-- Latest News -->

<hr />

<h1>Aktuellsten Neuigkeiten</h1>

<p>Noch nichts hier</p>
